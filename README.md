
# IoT Dashboard
<p align="center">
<a href="#"><img src="https://raw.githubusercontent.com/tortechnocom/iot-dashboard/master/public/img/IoTDashboard.png" alt="banner" width="50%" style="max-width:50%;"></a>
</p>


## System Requirements
1. Ubuntu 14.04LTS
2. Node.js
3. Npm
4. MongoDB

## Installation
1. Clone IoT Dashboard to your installation space: git clone git@gitlab.com:tortechnocom/iot-dashboard.git
2. Go to iot-dashboard directory and run: npm install
3. Start IoT Dashboard by using command: npm start
4. Go to browser and visit IoT Dashboard url: https://localhost:8443/

## Developing
Coming soon...

### Tools
- Atom 1.5.4

Created with Atom
