/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Thing = mongoose.model('Thing');
var JobSandbox = mongoose.model('JobSandbox');
var User = mongoose.model('User');
var schedule = require('node-schedule');
var utils = require('../../lib/utils');
var mqtt    = require('mqtt');
var jobs = [];

/**
 * Create user
 */

exports.create = function(req, res) {
	req.body.user_id = req.user._id;
	var thing = new Thing(req.body);
	var errJson = {};
	thing.save(function(err) {
		if (err) {
			console.log(err.message);
			errJson = err.errors;
			return res.json(err);
		} else {
			return res.json({});
		}
	});
};

exports.setting = function(req, res) {
	setLang(req, res);
	var things = null;
	Thing.findOne({
		_id : req.query.thing_id
	}, function(err, thing) {
		User.findOne({
			_id : req.query.user_id
		}, function(err, user) {
			jobs = JobSandbox.find({thing_id: thing._id}, function (err, jobs) {
				res.render('thing/thing-setting', {
					thing : thing,
					jobs: jobs,
					user : user
				});
			});
		});
	});
};

exports.update = function(req, res) {
	req.body.enabled = (req.body.enabled != null ? req.body.enabled : "N");
	console.log(req.body);
	jobParameters = [];
	for (parameter in req.body) {
		if (parameter.match("^JobSandbox")) {
				console.log('>>>>>>>> parameter: ', parameter);
				index = parseInt(parameter.split(":")[1]);
				if (!jobParameters[index]) {
					console.log('>>>>>>>> add default');
					jobParameters[index] = [];
				}
				jobParameters[index][parameter.split(':')[2]] = req.body[parameter];
		}
	}
	console.log('>>>>>>>>>>>>> jobParameters: ', jobParameters);
	var client = [];
	function callback(err, doc) {
		console.log("CallBack Job >>>>>>>>>>");
		if (err) {
			console.log(err.message);
			errJson = err.errors;
			return res.json(err);
		} else {
			console.log('>>>>>>> Run Job at: ' + doc.run_time);
			jobs[doc._id] = schedule.scheduleJob(doc.run_time, function() {
				console.log("Job [" + doc._id + "] " + doc.job_name);
				client[doc._id]  = mqtt.connect('mqtt://localhost:1883',
					{'username': "tortechnocom@hotmail.com", "password": "192438320ef88df981a8c52512c4d9d5be12832f",
					"clientId": "t:" + doc.thing_id});

				client[doc._id].on('connect', function () {
					client[doc._id].subscribe('iot/' + doc.thing_id);
					client[doc._id].publish('iot/' + doc.thing_id, doc.action);
				});

				client[doc._id].on('message', function (topic, message) {
					// message is Buffer
					console.log(">>>>>>>>> arrived msg: ", message.toString());
					Thing.findOneAndUpdate({
						_id : doc.thing_id
					}, {
						$set : {switch_status: message.toString()}
					}, function(err, doc) {
						if (err) {
							console.log(err.message);
						}
					});

					JobSandbox.findOneAndUpdate({
						_id : doc._id
					}, {
						$set : {thru_date: new Date()}
					}, function(err, doc) {
						if (err) {
							console.log(err.message);
						}
					});
					client.end();
				});
			});
		}
	}

	for (jobIndex in jobParameters) {
		console.log('>>> jobParameter: ', jobParameters[jobIndex]);
			job_name = req.body.thing_id + " - " + jobParameters[jobIndex]['run_time'];
			if (jobParameters[jobIndex]['_id']) {
				console.log('>>>>>>>>>> Existing job');
				var job = JobSandbox.findOneAndUpdate({
					_id : jobParameters[jobIndex]['_id']
				}, {
					$set : {job_name: job_name,
						run_time: jobParameters[jobIndex]['run_time'],
						status: jobParameters[jobIndex]['status'],
						action: jobParameters[jobIndex]['action'],
						schedule_type: jobParameters[jobIndex]['schedule_type'],
						thing_id: req.body.thing_id
					}
				}, function (err, doc) {
					callback(err, doc);
				});
			} else {
				var job = new JobSandbox({job_name: job_name,
					run_time: jobParameters[jobIndex]['run_time'],
					status: jobParameters[jobIndex]['status'],
					action: jobParameters[jobIndex]['action'],
					schedule_type: jobParameters[jobIndex]['schedule_type'],
					thing_id: req.body.thing_id,
					from_date: new Date()
				});
				job.save(function (err, doc) {
					callback(err, doc);
				});
			}
	}
	Thing.findOneAndUpdate({
		_id : req.body.thing_id
	}, {
		$set : req.body
	}, function(err, doc) {
		if (err) {
			console.log(err.message);
			errJson = err.errors;
			return res.json(err);
		} else {
			return res.json({});
		}
	});
};
