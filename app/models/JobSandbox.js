/**
 * Module dependencies.
 */
require("./default.js");

/**
 * Job Schema
 */

var JobSandboxSchema = new Schema({
    job_name: { type: String, default: '' },
    run_time: { type: Date, default: '' },
    action: { type: String, default: '' },
    status: {type: String, default: ''},
    thing_id: {type: String, default: ''},
    schedule_type: {type: String, default: ''},
    from_date: { type: Date, default: '' },
    thru_date: { type: Date, default: '' }
});
JobSandboxSchema.plugin(timestamps);

mongoose.model('JobSandbox', JobSandboxSchema);
